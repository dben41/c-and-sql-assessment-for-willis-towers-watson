#schema sql
|CREATE TABLE Employee(
  employeeId int,
  email varchar(255)
);

INSERT INTO Employee (employeeId, Email) VALUES (1, "bob.smith@somwhere.com"),
						(2, "eric.jones@somewhere.com"),
                                                (3, "Jill.brown@somewhere.com"),
                                                (4, "bob.smith@somwhere.com"),
                                                (5, "Jill.brown@somewhere.com"),
                                                (6, "Jill.brown@somewhere.com");

#query SQL
SELECT Email FROM Employee GROUP BY Email HAVING COUNT(*)>1;