#setup db
CREATE TABLE Department(
  departmentId int,
  name varchar(255),
  PRIMARY KEY (departmentId)
);

CREATE TABLE Employee(
  employeeId int,
  name varchar(255),
  salary int,
  departmentId int,
  PRIMARY KEY (employeeId),
  FOREIGN KEY (departmentId) REFERENCES Department(departmentId)
);

INSERT INTO Department (departmentId, name) VALUES (1, "Sales"),
												   (2, "Marketing");

INSERT INTO Employee (employeeId, name, salary, departmentId) VALUES (1, "Eric", 85000, 1),
												   				     (2, "Jill", 95000, 1),
                                                                     (3, "Bob", 76000, 2),
                                                                     (4, "Emily", 76000, 2),
                                                                     (5, "Sam", 75000, 2);

#the query
SELECT Department.name As Department, Employee.name AS Employee, Employee.Salary FROM Employee JOIN Department WHERE Employee.departmentId = Department.departmentId AND Employee.Salary IN
(SELECT max(salary)
   From Employee
   GROUP BY departmentId)