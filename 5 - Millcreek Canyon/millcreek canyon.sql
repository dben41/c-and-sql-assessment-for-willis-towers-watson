
/* Create Relational DBs */
CREATE TABLE DailyCanyonVisitors (
DailyVistorId INT NOT NULL AUTO_INCREMENT,
NumberOfVisitors INT,
VisitDate DATE,
PRIMARY KEY (DailyVistorId)
);


/*
Making the asssumption that customer data doesn't matter; as specs don't mention that. 
*/

CREATE TABLE Campsites (
CampsiteId INT NOT NULL AUTO_INCREMENT,
CampsiteName VARCHAR(255),
PRIMARY KEY (CampsiteId)
);

CREATE TABLE Dates (
DateId INT NOT NULL AUTO_INCREMENT,
CampsiteId INT NOT NULL,
Day DATE,      
Available BOOL,
PRIMARY KEY (DateId),
FOREIGN KEY (CampsiteId) REFERENCES Campsites(CampsiteId));   
                                              
/* Insert some dummy data, assuming that date range for application is April 1 - April 8, 2021 */
INSERT INTO DailyCanyonVisitors (NumberOfVisitors, VisitDate) VALUES ( 23, '2021-04-01'), 
                                              									  ( 35, '2021-04-02'),
                                              									  ( 45, '2021-04-03'),
                                                                                  ( 0, '2021-04-04'),
                                              									  ( 112, '2021-04-05'),
                                                                                  ( 129, '2021-04-06'),
                                              									  ( 54, '2021-04-07'),
                                                                                  ( 64, '2021-04-08');
                                              
INSERT INTO Campsites (CampsiteName) VALUES ("Sundance Site"), 
                                              ("Maple Mountain"),
                                              ("Foggy Bottom");
                                             
INSERT INTO Dates (CampsiteId, Day, Available ) VALUES ( 1, '2021-04-01', 1), 
                                                              ( 1, '2021-04-02', 0),
                                                              ( 1, '2021-04-03', 1),
                                                              ( 1, '2021-04-04', 0),
                                                              ( 1, '2021-04-05', 1),
                                                              ( 1, '2021-04-06', 0),
                                                              ( 1, '2021-04-07', 0),
                                                              ( 1, '2021-04-08', 0),
                                              				( 2, '2021-04-01', 0), 
                                                              ( 2, '2021-04-02', 1),
                                                              ( 2, '2021-04-03', 1),
                                                              ( 2, '2021-04-04', 0),
                                                              ( 2, '2021-04-05', 1),
                                                              ( 2, '2021-04-06', 0),
                                                              ( 2, '2021-04-07', 0),
                                                              ( 2, '2021-04-08', 0),
                                              				( 3, '2021-04-01', 1), 
                                                              ( 3, '2021-04-02', 0),
                                                              ( 3, '2021-04-03', 1),
                                                              ( 3, '2021-04-04', 1),
                                                              ( 3, '2021-04-05', 1),
                                                              ( 3, '2021-04-06', 0),
                                                              ( 3, '2021-04-07', 1),
                                                              ( 3, '2021-04-08', 1);
															  
/* Create stored procedures to add or cancel a reservation */
DELIMITER //

CREATE PROCEDURE AddReservation( IN _CampsiteId INT, IN _Day DATE)
BEGIN
	Update Dates SET Available = 0 WHERE CampsiteId = _CampsiteId AND Day = _Day;
END //

DELIMITER ;
DELIMITER //
CREATE PROCEDURE DeleteReservation( IN _CampsiteId INT, IN _Day DATE)
BEGIN
	Update Dates SET Available = 1 WHERE CampsiteId = _CampsiteId AND Day = _Day;
END //
DELIMITER ;

/* call stored procedure */
CALL AddReservation(3, '2021-04-08');
CALL DeleteReservation(1, '2021-04-02'); 

/* create view to show available reservation dates */
CREATE VIEW availableDates AS
    SELECT 
        CampsiteName,
        Day
    FROM
        Dates, Campsites
    WHERE Dates.CampsiteId = Campsites.CampsiteId
    AND Available = 1;
  
 /* call the view */ 
  SELECT * FROM availableDates		

/* 
Not sure how to fit the most popular day to visit the canyon into a function; from what I can see, a function is like a pipe method in angular; it transforms data and is useful in creating a new column. It seems like this would be better as stored procedure. 
Or maybe I just need more clarification on what is expected from the visitors in the canyon table.
*/  
SELECT NumberOfVisitors FROM DailyCanyonVisitors ORDER BY NumberOfVisitors DESC LIMIT 1;														  
															  
															  
                                                         